\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble }{}
\select@language {polish}
\select@language {english}
\etoc@startlocaltoc {1}
\contentsline {subsection}{\textbf {O APLIKACJI}}{4}{section*.2}
\contentsline {subsubsection}{Konfiguracja}{4}{section*.3}
\contentsline {subsubsection}{Instalacja}{4}{section*.4}
\contentsline {subsubsection}{Aktualizacja}{4}{section*.5}
\contentsline {subsection}{\textbf {LOGOWANIE}}{5}{section*.6}
\contentsline {subsubsection}{Struktura}{5}{section*.7}
\contentsline {subsubsection}{Logowanie}{5}{section*.8}
\contentsline {subsubsection}{Zarz\IeC {\k a}dzanie u\IeC {\.z}ytkownikami}{7}{section*.9}
\contentsline {subsection}{\textbf {ZARZ\IeC {\k A}DZANIE ETYKIETAMI}}{8}{section*.10}
\contentsline {subsubsection}{Przegl\IeC {\k a}danie}{8}{section*.11}
\contentsline {subsubsection}{Filtrowanie}{9}{section*.12}
\contentsline {subsubsection}{Dodawanie i edycja rekord\IeC {\'o}w}{9}{section*.13}
\contentsline {subsubsection}{Zakres i opcje generowania}{9}{section*.14}
\contentsline {subsubsection}{Import danych}{10}{section*.15}
\contentsline {subsubsection}{Eksport}{11}{section*.16}
\contentsline {subsubsection}{Usuwanie danych}{11}{section*.17}
\contentsline {subsection}{\textbf {GENEROWANIE I WYDRUK}}{12}{section*.18}
\contentsline {subsubsection}{Uk\IeC {\l }ad wydruku}{12}{section*.19}
\contentsline {subsubsection}{Konfiguracja wydruku}{14}{section*.20}
\contentsline {subsubsection}{Komunikacja z drukark\IeC {\k a}}{14}{section*.21}
\contentsline {subsubsection}{Przyk\IeC {\l }ad etykiet}{16}{section*.22}
\contentsline {subsection}{\textbf {SPIS RYSUNK\IeC {\'O}W}}{17}{section*.23}
